const readline = require('readline-sync');
const utils = require('./libs/utils')

const text = readline.question("input:\t");
console.log("output:\t" + utils.longestSubstringWithoutRepeatingCharacters(text))
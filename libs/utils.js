module.exports = {
    max58num: num => {
        const numSplit = num.split("")
        let isChange = false
        const result = numSplit.map(num => {
            if(!isChange && num == '5') {
                isChange = true
                return '8'
            }
            return num
        })
        return result.join('')
    },    
    getLongestWord: wordArray => {
        let longestWord = wordArray[0]
        wordArray.forEach(word => {
            if(longestWord.length < word.length)
                longestWord = word
        });
        return longestWord
    },
    longestSubstringWithoutRepeatingCharacters: text => {
        const textArr = text.split('')
        let allSubstring = []
        for(let i = 0; i < textArr.length; i++) {
            let tempSubstring = []
            for(let j = i; j < textArr.length; j++) {
                if(!tempSubstring.includes(textArr[j])) {
                    tempSubstring.push(textArr[j])
                    if(j == textArr.length - 1)
                        allSubstring.push(tempSubstring.join(''))
                } else {
                    allSubstring.push(tempSubstring.join(''))
                    break;
                }
            }
        }
        return module.exports.getLongestWord(allSubstring)
    }
}
